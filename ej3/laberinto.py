# -*- coding: utf-8 -*-
import random



class Laberinto(object):


    def __init__(self, parent=None):
        self.parent = parent
        self.lab = []
        self.tam = 0,0
        self.queso = 0,0	
        self.rata = 0,0
        self.visitada = []
        self.caminoActual = []


    def cargar(self, fn):
        with open(fn) as a:
            a.readline()
            self.lab = [self._leerFila(fila) for fila in a]
            self.tam = len(self.lab), len(self.lab[0])
            self.resetear()


    def tamano(self):
        return self.tam


    def getPosicionRata(self):
        return self.rata


    def setPosicionRata(self,i,j):
        if i < self.tam[0] and j < self.tam[1]:
            self.rata = i,j
            return True
        return False


    def getPosicionQueso(self):
        return self.queso


    def setPosicionQueso(self,i,j):
        if i < self.tam[0] and j < self.tam[1]:
            self.queso = i,j
            return True
        return False


    def esPosicionRata(self,i,j):
        return i == self.rata[0] and j == self.rata[1]


    def esPosicionQueso(self,i,j):
        return i == self.queso[0] and j == self.queso[1]


    def get(self,i,j):
        return self.lab[i][j]


    def getInfoCelda(self,i,j):
        return {
            'visitada': self.visitada[i][j],
            'caminoActual': (i,j) in self.caminoActual
        }


    def resuelto(self):
        return self.rata == self.queso


    def resolver(self):
        while not self.resuelto() and not self._fracaso():
            self._status()
            self._moverRata()
            self._redibujar()
        self._status()
        return self.resuelto()


    def _leerFila(self, fila):
        fil = fila.strip('[]\n').split('][')
        return [self._leerCelda(c) for c in fil]


    def _leerCelda(self, celda):
        return [v == '1' for v in celda.split(',')]  


    def _fracaso(self):
        v = self._vecinasAccesiblesNoVisitadas()
        i,j = self.rata
        return len(v)==0 and i==0 and j==0


    def _moverRata(self):
        v = self._vecinasAccesiblesNoVisitadas()
        if len(v) > 0:
            celda = random.choice(v)
            self._visitar(celda[0],celda[1])
        else:
            self._retroceder()


    def _retroceder(self):
        self.caminoActual.pop()
        self.rata = self.caminoActual[-1]


    def _visitar(self,i,j):
        self.rata = i,j
        self.caminoActual.append((i,j))
        self.visitada[i][j] = True


    def _vecinasAccesiblesNoVisitadas(self):
        return [v for v in self._vecinasAccesibles() if v not in self._vecinasVisitadas()]


    def _vecinasAccesiblesVisitadas(self):
        return [v for v in self._vecinasAccesibles() if v in self._vecinasVisitadas()]


    def _vecinasAccesibles(self):
        i,j = self.rata
        vecinas = [(i,j-1),(i-1,j),(i,j+1),(i+1,j)]
        celda = self.lab[i][j]
        accesibles = [vecinas[k] for k in range(4) if not celda[k]]
        return [a for a in accesibles if self._celdaValida(a[0],a[1])]


    def _vecinasVisitadas(self):
        return [v for v in self._vecinas() if self.visitada[v[0]][v[1]]]


    def _vecinas(self):
        i,j = self.rata
        vecinas = [(i,j-1),(i-1,j),(i,j+1),(i+1,j)]
        return [v for v in vecinas if self._celdaValida(v[0],v[1])]


    def _celdaValida(self,i,j):
        return i<self.tam[0] and j<self.tam[1] and i>=0 and j>=0


    def resetear(self):
        n,m = self.tam
        self.queso = n-1, m-1
        self.visitada = [[False for p in range(m)] for q in range(n)]
        self.caminoActual = []
        self._visitar(0,0)


    def _redibujar(self):
        if self.parent is not None:
            self.parent.update()


    def _status(self):
        va = self._vecinasAccesibles()
        vanv = self._vecinasAccesiblesNoVisitadas()
        vav = self._vecinasAccesiblesVisitadas()
        print('rata: ' + str(self.rata))
        print('accesibles: ' + str(va))
        print('accesibles no visitadas: ' + str(vanv))
        print('accesibles visitadas: ' + str(vav))
        print('camino: ' + str(self.caminoActual))
        print('-'*60)
