Este programa toma todas las imagenes almacenadas en DIR_INPUT,
les aplica un filtro de escala de grises, seguido de un blur, y guarda
el resultado en un nuevo archivo en DIR_OUTPUT.

Se mide el tiempo de procesamiento de la aplicacion de estos filtros 
para cada imagen, variando el uso de JIT (Just in Time Compilation) y
la cantidad de cores utilizados en el caso de habilitar paralelismo.

Los resultados de estas mediciones se imprimen en consola al ejecutar
el programa, y además se genera un plot grafico con los tiempos
de distintos cores utilizados. 

Las distintas variables posibles se encuentran en config.py, y son:

* INCLUIR_TIEMPO_SIN_JIT
    Valor booleano indicando si se desea incluir tiempo de compilacion
    sin JIT. Es util deshabilitarlo para el caso de imagenes de 
    gran tamaño, si se desea analizar unicamente el rendimiento de 
    paralelismo, ya que esto tomaria demasiado tiempo.

* DIR_INPUT 
    Directorio de las imagenes de entrada.

* DIR_OUTPUT
    Directorio de las imagenes de salida.

* CORES 
    Lista con todas las cantidades de cores posibles. Por ejemplo,
    si la maquina posee 8 cores y se desea analizar el 
    rendimiento de todos los valores posibles, la lista seria [1,2,4,8]. 
    Si solo se desea analizar el rendimiento cuando se usan 4, 
    la lista seria [4]
