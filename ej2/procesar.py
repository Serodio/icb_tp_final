import os
import imageio
from filtros import filtros
from config import INCLUIR_TIEMPO_SIN_JIT
from config import DIR_INPUT 
from config import DIR_OUTPUT
from config import CORES



def tiempoConJIT(f, img, cores):
    os.environ['NUMBA_NUM_THREADS'] = str(cores)
    img_final, tiempo = filtros(img, True)
    print('\tcores: ' + str(cores) + '\ttiempo: ' + str(tiempo) + ' s')
    imageio.imsave(DIR_OUTPUT + str(cores) + 'cores_' + f, img_final)
    return tiempo



def tiempoSinJIT(f, img):
    img_final, tiempo = filtros(img, False)
    imageio.imsave(DIR_OUTPUT + 'sinJIT_' + f, img_final)
    print('\tsin jit\t\ttiempo: ' + str(tiempo) + ' s')
    return tiempo



def procesar_archivo(f):
    # procesar un archivo con las distintas variantes de paralelismo posibles
    path = os.path.join(DIR_INPUT, f)
    img = imageio.imread(path)
    dimensiones = str(len(img)) + 'x' + str(len(img[0])) + ' pixels'
    print('\n' + f + ': ' + dimensiones)
    res = {}
    if INCLUIR_TIEMPO_SIN_JIT:
        res['tiempoSinJIT'] = tiempoSinJIT(f,img)
    res['nombre'] = f
    res['dim_x'] = len(img)
    res['dim_y'] = len(img[0])
    res['tiempoConJIT'] = {str(c):tiempoConJIT(f,img,c) for c in CORES}
    return res



def primera_compilacion():
    # ejecutar por primera vez para luego no incluir el tiempo de compilacion
    archivos = os.listdir(DIR_INPUT)
    path = os.path.join(DIR_INPUT, archivos[0])
    img = imageio.imread(path)
    filtros(img, True)
