import time
from numba import jit
import numpy as np



@jit(parallel=True)
def gray_filter(a):
    dim = len(a), len(a[0]) 
    res = np.zeros(dim, dtype=np.uint8)
    for i in range(dim[0]):
        for j in range(dim[1]):
            res[i,j] = 0.3*a[i,j][0] + 0.6*a[i,j][1] + 0.11*a[i,j][2]
    return res



@jit(parallel=True)
def blur_filter(a):
    dim = len(a), len(a[0]) 
    res = np.zeros(dim, dtype=np.uint8)
    for i in range(1,dim[0]-1):
        for j in range(1,dim[1]-1):
            suma = int(a[i-1,j]) + int(a[i+1,j]) + int(a[i,j-1]) + int(a[i,j+1])
            res[i,j] = suma/4
    return res



def filtros(img1,jit):
    t1 = time.time()
    if jit:
        img2 = gray_filter(img1)
        img3 = blur_filter(img2)
    else:
        img2 = gray_filter.py_func(img1)
        img3 = blur_filter.py_func(img2)
    t2 = time.time()
    return img3, round(t2-t1,5)
