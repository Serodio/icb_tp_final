import os
from plot import plot
from filtros import filtros
from config import DIR_INPUT
from procesar import primera_compilacion
from procesar import procesar_archivo



if __name__ == '__main__':
    archivos = os.listdir(DIR_INPUT)
    archivos.sort()
    primera_compilacion()
    performance = [procesar_archivo(f) for f in archivos]
    plot(performance)
