import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator



def minmax_tiempo(imagenes):
    tmax = 0
    tmin = 10**10
    for i in imagenes:
        tmax_i = max(i['tiempoConJIT'].values())
        tmin_i = max(i['tiempoConJIT'].values())
        if tmax_i > tmax:
            tmax = tmax_i
        if tmin_i < tmin:
            tmin = tmin_i
    return tmax, tmin



def plot(imagenes):
    ax = plt.figure().gca()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ymax, ymin = minmax_tiempo(imagenes)
    ax.set_ylim(ymin*0.9, ymax*1.3)
    for i in imagenes:
        dim = str(i['dim_x']) + 'x' + str(i['dim_y'])
        label = i['nombre'] + ' (' + dim + ')'
        x = [float(k) for k in i['tiempoConJIT'].keys()]
        y = [float(k) for k in i['tiempoConJIT'].values()]
        ax.plot(x, y, label=label, marker='o')
    ax.grid(which='major', axis='both')
    plt.xlabel('cores')
    plt.ylabel('tiempo')
    plt.legend(loc='upper right')
    plt.show()
