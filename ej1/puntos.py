import random



X_MAX = 1000
Y_MAX = 1000



def dx(tupla):
    return abs(tupla[1][0] - tupla[0][0])



def dy(tupla): 
    return abs(tupla[0][1] - tupla[1][1])



def distancia(tupla):
    return (dx(tupla)**2 + dy(tupla)**2)**0.5



def menorDistancia(pares):
    par_min = pares[0]
    for par in pares:
        if distancia(par) < distancia(par_min):
            par_min = par
    return par_min



def listaDePuntos(fn):
    with open(fn) as f:
        lista = []
        for linea in f:
            linea = linea.strip().split()
            linea = float(linea[0]), float(linea[1])
            lista.append(linea)
        return lista



def generarPunto():
    x = (random.random()*2 - 1) * X_MAX
    y = (random.random()*2 - 1) * Y_MAX
    return round(x,2), round(y,2)



def generarLista(cantidad):
    return [generarPunto() for i in range(cantidad)]
