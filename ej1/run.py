from plot import Algoritmo
from plot import ListaDeAlgoritmos



if __name__ == '__main__':

    algoritmos = ListaDeAlgoritmos([
        Algoritmo('iter','Iterativo','D'),
        Algoritmo('up','Recursivo - upSort','o'),
        Algoritmo('sort','Recursivo - Python Sort','s'),
        Algoritmo('merge','Recursivo - mergeSort','^')
    ])

    algoritmos.ejecutar()

    algoritmos.plot()
