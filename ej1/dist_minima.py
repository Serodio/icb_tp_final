from puntos import distancia, dx, menorDistancia
from ordenar import ordenar



def parMinimoCruzado(lista, dmax):
    m = len(lista)//2
    par_min = lista[m-1], lista[m] 
    i = m-1
    while i >= 0 and dx((lista[i],lista[m])) < dmax:
        j = m
        while j < len(lista) and dx((lista[j],lista[m])) < dmax:
            par = lista[i], lista[j] 
            par_min = menorDistancia([par, par_min])
            j += 1
        i -=1
    return par_min



def parMinimo(lista):
    if len(lista) >= 4:
        mitad1 = lista[:len(lista)//2]
        mitad2 = lista[len(lista)//2+1:] if len(lista)%2 else lista[len(lista)//2:]
        par1 = parMinimo(mitad1)
        par2 = parMinimo(mitad2)
        par3 = parMinimoCruzado(lista, min(distancia(par1), distancia(par2))) 
        return menorDistancia([par1, par2, par3])
    else:
        return lista



def distanciaMinimaDyC(l, alg_sort):
    # algoritmo recursivo
    lista = ordenar(l, alg_sort)
    return parMinimo(lista)



def distanciaMinima(l):
    # algoritmo iterativo
    par_min = l[0], l[1]
    dis_min = distancia(par_min)
    for t1 in l:
        for t2 in l:
            if t1 != t2:
                dis = distancia((t1, t2))
                if dis < dis_min:
                    dis_min = dis
                    par_min = t1, t2
    return par_min
