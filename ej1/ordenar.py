def merge(lista,mitad1,mitad2):
    i=0
    j=0
    k=0
    while i < len(mitad1) and j < len(mitad2):
        if mitad1[i][0] <= mitad2[j][0]:
            lista[k]=mitad1[i]
            i=i+1
        else:
            lista[k]=mitad2[j]
            j=j+1
        k=k+1
    while i < len(mitad1):
        lista[k]=mitad1[i]
        i=i+1
        k=k+1
    while j < len(mitad2):
        lista[k]=mitad2[j]
        j=j+1
        k=k+1



def mergeSort(lista):
    if len(lista)>1:
        mitad = len(lista)//2
        mitad1 = lista[:mitad]
        mitad2 = lista[mitad:]
        mergeSort(mitad1)
        mergeSort(mitad2)
        merge(lista,mitad1,mitad2)
    return lista



def maxPosicion(lista,i):
    m = 0
    for k in range(0,i+1):
        if lista[k][0] > lista[m][0]:
            m = k
    return m



def swapMax(lista,i,m):
    if lista[m][0] > lista[i][0]:
        aux = lista[i]
        lista[i] = lista[m]
        lista[m] = aux



def upSort(lista):
    i = len(lista)-1
    while i > 0:
        m = maxPosicion(lista,i)
        swapMax(lista,i,m)
        i -= 1
    return lista



def ordenar(lista, alg_sort):
    if alg_sort == 'merge':
        return mergeSort(lista)
    if alg_sort == 'up':
        return upSort(lista)
    if alg_sort == 'sort':
        lista.sort()
        return lista
