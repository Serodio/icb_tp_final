import time
import matplotlib.pyplot as plt
from puntos import generarLista
from dist_minima import distanciaMinimaDyC
from dist_minima import distanciaMinima
from config import TAM_MUESTRAS



class Algoritmo:
    '''
    Clase que maneja un Algoritmo individual.
    Cada vez que se ejecuta con una lista com dato de entrada,
    almacena el tiempo de ejecucion y el tamaño de la lista
    respectivo.
    '''

    def __init__(self, nombre, label, marker):
        self.nombre = nombre
        self.label = label
        self.tiempos = []
        self.tam_muestras = []
        self.marker = marker

    def ejecutar(self, lista):
        t1 = time.time()
        if self.nombre == 'iter':
            distanciaMinima(lista)
        else:
            distanciaMinimaDyC(lista, self.nombre)
        t2 = time.time()
        tiempo = round(t2-t1,4)
        self.tam_muestras.append(len(lista))
        self.tiempos.append(tiempo)
        return tiempo



class ListaDeAlgoritmos():
    '''
    Clase que contiene una lista de Algoritmos.
    Maneja la ejecucion y el ploteo de todos ellos en serie.
    '''

    def __init__(self, algoritmos):
        self.algoritmos = algoritmos

    def ejecutar(self):
        for tam in TAM_MUESTRAS:
            lista = generarLista(tam)
            print('\nMuestra de tamaño: ' + str(tam))
            for a in self.algoritmos:
                tiempo = a.ejecutar(lista)
                print('\t'+ a.label + ': ' + str(tiempo) + 's')

    def plot(self):
        for a in self.algoritmos:
            plt.plot(a.tam_muestras, a.tiempos, label=a.label, marker=a.marker)
        plt.grid(which='major', axis='both')
        plt.xlabel('tamaño de muestra')
        plt.ylabel('tiempo')
        plt.legend()
        plt.show()
