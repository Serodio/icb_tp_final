Este programa mide el tiempo de ejecucion de distintos algoritmos sobre 
los mismos datos de entrada, variando el tamaño de la muestra.

Los datos de entrada son generados dinámicamente por el programa.

Los tamaños de muestras se pueden configurar con la variable TAM_MUESTRAS 
del archivo config.py. Por ejemplo, si se desea probar cada uno de los 
algorimos con listas de datos aleatorios de longitud 100, 200 y 300, 
TAM_MUESTRAS = [100,200,300], lo cual tambien puede indicarse utilizando 
la funcion 'range'.
