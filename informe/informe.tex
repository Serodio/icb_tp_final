\documentclass[12pt]{article}
\usepackage{listings}
\usepackage{titlesec}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{indentfirst}
\usepackage{subcaption}
\usepackage[margin=3cm]{geometry}
\setlength{\parskip}{0.4\baselineskip}
\setlength{\parindent}{0.5cm}


\lstset{
  aboveskip=0.4cm,
  belowskip=0.4cm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  frame=none,
  numbers=none,
  numberstyle=\tiny\color{gray},
  breakatwhitespace=true,
  tabsize=3,
  xleftmargin=0.7cm,
  xrightmargin=0.7cm
}


\titlespacing*{\section}
{0pt}{0pt}{2\baselineskip}


\titlespacing*{\subsection}
{0pt}{2\baselineskip}{0.8\baselineskip}



\begin{document}


\begin{center} 
    \vspace*{\fill}
    {\huge Informe de Trabajo Práctico} \\
    \bigskip
    \bigskip
    {\large Introducción a la Computación} \\
    \medskip
    {\large Tópicos de Programación}\\
    \bigskip
    \bigskip
    \bigskip
    \bigskip
    {\large \textbf{Alumno:} Manuel Serodio} \\ 
    \bigskip
    {\large \textbf{Email:} manuelserodio.arg@gmail.com} \\
    \vspace*{\fill}
\end{center}




\newpage
\section*{Ejercicio 1: Divide \& Conquer}


\subsection*{Descripción general}

Este programa mide el tiempo de ejecución de distintos algoritmos que resuelven el mismo problema, sobre los mismos datos de entrada, produciendo la misma salida. Se evalúa además cada algoritmo variando el tamaño de los datos de entrada.
El problema a resolver consiste en seleccionar dos puntos del plano 2D entre una lista de puntos provista como dato de entrada. La condición a cumplir es que el par seleccionado debe ser el par con la distancia mínima entre sí, de todas las combinaciones de pares posibles.
Los algoritmos a probar son cuatro:

\begin{enumerate}
    \item Iterativo (fuerza bruta)
    \item Recursivo, utilizando upSort 
    \item Recursivo, utilizando mergeSort
    \item Recursivo, utilizando el sort de Python
\end{enumerate}


\subsection*{Decisiones de diseño}

El código se divide en los siguientes archivos:

\begin{enumerate}
    \item \textbf{config.py}: Configuración de variables globales.
    \item \textbf{run.py}: Archivo que ejecuta todo el programa, generando output con los resultados en la consola y un plot gráfico con la misma información. Contiene además la lista de algoritmos a utilizar, los cuales pueden deshabilitarse individualmente marcándolos como comentario.  
    \item \textbf{plot.py}: Archivo encargado de la ejecución de los algoritmos y de realizar el plot de salida. Contiene dos clases, \textit{Algoritmo} y \textit{ListaDeAlgoritmos}. Un objeto de clase \textit{ListaDeAlgoritmos} genera dinámicamente una muestra de cierto tamaño (sin abrirla ni guardarla en un archivo, aunque esta función se encuentra implementada en caso de ser necesaria). Luego, ejecuta todos los algoritmos de la lista sobre esa muestra, genera una nueva muestra del siguiente tamaño, y así sucesivamente hasta terminar con todos los tamaños de muestra especificados, cuando realiza el plot. Cada objeto de clase \textit{Algoritmo} se encarga de su propia ejecución, de medir el tiempo, y almacenar este último dentro de sus atributos internos.
    \item \textbf{dist\_minima.py}: Algoritmos iterativo y recursivo, la parte central del ejercicio.
    \item \textbf{ordenar.py}: Algoritmos de ordenamiento upSort y mergeSort, junto con sus respectivas funciones auxiliares.
    \item \textbf{puntos.py}: Archivo relacionado específicamente con puntos. Por ejemplo, generación de una lista de puntos al azar como datos de entrada, lectura de un archivo de puntos, cálculo de la distancia entre dos puntos, y pequeñas funciones auxiliares.
\end{enumerate}


\subsection*{Resultados}

Se realizaron dos plots gráficos utilizando Matplotlib, uno incluyendo el algoritmo iterativo y otro excluyéndolo, debido a la diferencia notable de tiempo. Los tamaños de muestras en el archvo \textbf{config.py} fueron los siguientes:

\begin{lstlisting}[language=Python]
TAM_MUESTRAS = range(500,10000,1500)
\end{lstlisting}

Como cabe esperar, el algoritmo iterativo es el que más tiempo toma en ejecutarse. Dentro de los algoritmos recursivos, el que sufre el peor rendimiento es el que utiliza el método upSort. Los métodos mergeSort y el sort de Python ofrecen un rendimiento muy similar, con una leve ventaja del sort de Python.
Ambos plots puden observarse en la \textbf{Figura 1} al final de este informe. Además, el programa ofrece automáticamente la misma información en formato de texto como output en la consola:

\newpage
\begin{lstlisting}[language=bash]
Muestra de tamano: 500
	Iterativo: 0.7831s
	Recursivo - upSort: 0.0789s
	Recursivo - Python Sort: 0.0386s
	Recursivo - mergeSort: 0.044s

Muestra de tamano: 2000
	Iterativo: 12.406s
	Recursivo - upSort: 0.9289s
	Recursivo - Python Sort: 0.2541s
	Recursivo - mergeSort: 0.2767s

Muestra de tamano: 3500
	Iterativo: 37.5835s
	Recursivo - upSort: 2.5645s
	Recursivo - Python Sort: 0.5032s
	Recursivo - mergeSort: 0.5597s

Muestra de tamano: 5000
	Iterativo: 77.0507s
	Recursivo - upSort: 5.3375s
	Recursivo - Python Sort: 0.826s
	Recursivo - mergeSort: 0.8915s

Muestra de tamano: 6500
	Iterativo: 132.1767s
	Recursivo - upSort: 9.0131s
	Recursivo - Python Sort: 1.0966s
	Recursivo - mergeSort: 1.2073s

Muestra de tamano: 8000
	Iterativo: 202.0088s
	Recursivo - upSort: 14.0518s
	Recursivo - Python Sort: 1.4806s
	Recursivo - mergeSort: 1.6147s

Muestra de tamano: 9500
	Iterativo: 291.9539s
	Recursivo - upSort: 25.797s
	Recursivo - Python Sort: 1.9332s
	Recursivo - mergeSort: 2.1265s
\end{lstlisting}


\subsection*{Cómo ejecutar el código}

Para este trabajo se hizo uso de las herramientas pip, virtualenv y virtualenvwrapper. Todos los módulos de Python utilizados se listan en el archivo \textbf{requirements.txt}, y pueden instalarse con el comando: 

\begin{lstlisting}[language=bash]
=> pip install -r requirements.txt
\end{lstlisting}

Para ejecutar el código se debe posicionar en el directorio \textbf{ej1} y ejecutar el comando:

\begin{lstlisting}[language=bash]
=> python3 run.py
\end{lstlisting}

En el archivo \textbf{config.py} se encuentra una variable llamada \textbf{tam\_muestras}, cuyo valor es una lista que especifica los tamaños de muestras a evaluar. Por ejemplo, si se desea ejecutar cada uno de los algorimos con listas de datos aleatorios de longitud 100, 200 y 300, se puede configurar: 

\begin{lstlisting}[language=Python]
TAM_MUESTRAS = [100,200,300]
\end{lstlisting}

Lo cual también puede indicarse utilizando la función \textbf{range}.

Por último, es posible seleccionar cuáles algoritmos ejecutar y cuáles no. Esto es útil especialmente debido al tiempo que demora el algoritmo iterativo, y puede resultar práctico deshabilitarlo para evaluar rápidamente sólo los algoritmos recursivos. Esto se logra editando el archivo \textbf{run.py}, poniendo en comentario las lineas correspondientes a los algoritmos que se desea omitir.



\newpage
\section*{Ejercicio 2: Python \& Numba}


\subsection*{Descripción general}

El objetivo de este ejercicio es analizar el rendimiento de algoritmos cuando se aplica procesamiento paralelo.
Este programa toma todas las imágenes almacenadas en un directorio, les aplica un filtro de escala de grises, seguido de un filtro de blur, y guarda la imagen resultante en un nuevo archivo. 
Se mide el tiempo de procesamiento de la aplicacion de estos filtros para cada imagen, con las siguientes variantes:

\begin{enumerate}
    \item Sin uso de JIT (Just in Time Compilation) ni paralelismo.
    \item Habilitando JIT, sin paralelismo (utilizando un solo núcleo).
    \item Habilitando JIT, con paralelismo, utilizando dos núcleos.
    \item Habilitando JIT, con paralelismo, utilizando mayor cantidad de núcleos (no se utilizaron debido a no estar presentes en el hardware disponible).
\end{enumerate}


\subsection*{Decisiones de diseño}

El código se divide en los siguientes archivos:

\begin{enumerate}
    \item \textbf{config.py}: Configuración de variables globales.
    \item \textbf{run.py}: Archivo que ejecuta todo el programa. Toma todas las imagenes almacenadas en un directorio, invoca la función que las procesa, y luego invoca la función que realiza el plot de los resultados.
    \item \textbf{procesar.py}: Funciones que toman los archivos de imágenes provistos por \textbf{run.py}, les aplican los filtros y retornan el resultado (dimensiones de imagen y tiempo de ejecución para cada cantidad distinta de núcleos utilizados).
    \item \textbf{filtros.py}: gray\_filter, blur\_filter, y una función que los envuelve para medir su tiempo de ejecución conjunto, habilitando o deshabilitando JIT y paralelismo según corresponda. 
    \item \textbf{plot.py}: Función que recibe los tiempos obtenidos y realiza el plot utilizando Matplotlib. 
\end{enumerate}


\subsection*{Notas}

Para cambiar dinámicamente (en tiempo de ejecución) la cantidad de núcleos se utiliza la siguiente línea:

\begin{lstlisting}[language=bash]
os.environ['NUMBA_NUM_THREADS'] = str(cores)
\end{lstlisting}

Y para deshabilitar dinámicamente los decoradores de numba de JIT, se llama la función de filtro del siguiente modo:

\begin{lstlisting}[language=bash]
gray_filter.py_func(imagen)
\end{lstlisting}


\subsection*{Resultados}

Se observa una diferencia de tiempo considerable al habilitar o deshabilitar JIT (Just in Time Compilation), tal como era de esperarse. 

Sin embargo, una vez habilitado JIT, no se aprecia una diferencia de tiempo consistente y notable entre utilizar uno o dos núcleos. La diferencia de tiempo parece perderse dentro del margen de error, y únicamente en las imágenes más grandes (6000x6000 pixeles) puede notarse una mejoría repetidamente, que aún así no es demasiado grande.

En la \textbf{Figura2}, al final de este informe, puede apreciarse una imagen antes y después de la aplicacion de los filtros. El resultado obtenido con las distintas variantes (sin JIT, con JIT, con distinta cantidad de núcleos) es exactamente el mismo, con la única diferencia del tiempo requerido.

En la \textbf{Figura3} se grafica el tiempo de cómputo en función de la cantidad de núcleos utilizados, para imágenes de distinto tamaño.

Además, mientras el programa se ejecuta se imprime en pantalla la misma información que se grafica luego en el plot. A continuación se incluye una muestra que corresponde a un experimento diferente del plot de la \textbf{Figura3}, con imágenes de distintos tamaños, por una cuestión de practicidad (procesar imágenes grandes sin JIT toma demasiado tiempo).


\newpage
\begin{lstlisting}[language=bash]
100.jpg: 100x100 pixels
	sin jit     tiempo: 1.52015 s
	cores: 1    tiempo: 0.00039 s
	cores: 2    tiempo: 0.00039 s

200.jpg: 200x200 pixels
	sin jit     tiempo: 12.90929 s
	cores: 1    tiempo: 0.00116 s
	cores: 2    tiempo: 0.0012 s

300.jpg: 300x300 pixels
	sin jit     tiempo: 29.15163 s
	cores: 1    tiempo: 0.00247 s
	cores: 2    tiempo: 0.00251 s

400.jpg: 400x400 pixels
	sin jit     tiempo: 24.61068 s
	cores: 1    tiempo: 0.00502 s
	cores: 2    tiempo: 0.00499 s
\end{lstlisting}



\subsection*{Cómo ejecutar el código}

Del mismo modo que en el ejercicio anterior, todos los módulos de Python utilizados se listan en el archivo \textbf{requirements.txt}. Para ejecutar el código se debe posicionar en el directorio \textbf{ej2} y ejecutar el comando:

\begin{lstlisting}[language=bash]
=> python3 run.py
\end{lstlisting}

En el archivo \textbf{config.py} se encuentran las siguientes variables:

\begin{enumerate}
    \item \textbf{incluir\_tiempo\_sin\_jit}: Valor booleano indicando si se desea incluir tiempo de compilación sin JIT. Es util deshabilitarlo para el caso de imágenes de gran tamaño, si se desea analizar únicamente el rendimiento de paralelismo, ya que incluirlo sería innecesario y además tomaría demasiado tiempo.
    \item \textbf{dir\_input}: Directorio de las imágenes de entrada.
    \item \textbf{dir\_output}: Directorio de las imágenes de salida.
    \item \textbf{cores}: Lista con todas las cantidades de núcleos posibles. Por ejemplo, si la máquina posee 8 núcleos y se desea analizar el rendimiento de todos los valores posibles, la lista sería [1,2,4,8]. Si sólo se desea analizar el rendimiento cuando se usan 4, la lista sería [4]
\end{enumerate}



\newpage
\section*{Ejercicio 3: Backtracking}


\subsection*{Descripción general}

El objetivo de este ejercicio es resolver un laberinto por medio de backtracking. El enunciado provee una interfaz gráfica y la estructura para comunicarse con esa interfaz. Lo que debe hacerse es la implementación interna de la clase Laberinto.


\subsection*{Decisiones de diseño}

Todo el código implementado se encuentra en un sólo archivo, \textbf{laberinto.py}.

Por una cuestión de simpleza se eligió un enfoque iterativo en lugar de recursivo.

Tanto el laberinto como las celdas visitadas se cargan en una matriz (lista de listas). El camino recorrido, sin embargo, se carga como una lista. Esto permite desandar el camino con suma facilidad (moviendo la rata a la anteúltima posición de la lista, y eliminando la última).


\subsection*{Método de carga}

El código que se ocupa de la carga de datos es la siguiente:

\begin{lstlisting}[language=Bash]
def __init__(self, parent=None):
    self.parent = parent
    self.lab = []
    self.tam = 0,0
    self.queso = 0,0	
    self.rata = 0,0
    self.visitada = []
    self.caminoActual = []


def cargar(self, fn):
    with open(fn) as a:
        a.readline()
        self.lab = [self._leerFila(fila) for fila in a]
        self.tam = len(self.lab), len(self.lab[0])
        self.resetear()


def _leerFila(self, fila):
    fil = fila.strip('[]\n').split('][')
    return [self._leerCelda(c) for c in fil]


def _leerCelda(self, celda):
    return [v == '1' for v in celda.split(',')]


def resetear(self):
    n,m = self.tam
    self.queso = n-1, m-1
    self.visitada = [[False for p in range(m)] for q in range(n)]
    self.caminoActual = []
    self._visitar(0,0)
\end{lstlisting}



\subsection*{Bucle principal}

El método principal es un sencillo bucle while:

\begin{lstlisting}[language=Bash]
def resolver(self):
    while not self.resuelto() and not self._fracaso():
        self._status()
        self._moverRata()
        self._redibujar()
    self._status()
    return self.resuelto()


def _moverRata(self):
    v = self._vecinasAccesiblesNoVisitadas()
    if len(v) > 0:
        celda = random.choice(v)
        self._visitar(celda[0],celda[1])
    else:
        self._retroceder()
\end{lstlisting}

El propósito del método \textbf{status} es debugging, y se explicará próximamente. 

El resto de los métodos, al ser bastante breves y tener nombres autodescriptivos, pueden observarse directamente en el código. 


\subsection*{Ejecución}

Tal como es indicado por el enunciado, el código se ejecuta simplemente con:

\begin{lstlisting}[language=Bash]
=> python3 tplaberinto.py
\end{lstlisting}

La única salvedad es que, a diferencia de los ejercicios anteriores, se requiere desactivar virtualenv. Esto se debe a que el código no hace uso de módulos especiales, y además necesita acceder a módulos del sistema.


\subsection*{Debugging}

Este ejercicio presentó una dificultad adicional, ya que se ejecuta mediante interfaz gráfica en lugar de texto, lo cual conlleva que es difícil de debuggear.

En particular, varias veces se observaba el comportamiento de la rata en el laberinto, que comenzaba correctamente, pero luego de cierto tiempo fallaba de modos extraños. Por ejemplo:

\begin{enumerate}
    \item Atravezaba paredes de la última columna, pero no de otras.
    \item Retrocedía sobre su propio camino de modo apropiado cuando debía hacerlo, pero al llegar a un lugar con celdas vecinas aún no visitadas, continuaba retrocediendo en lugar de tomar caminos inexplorados.
    \item Se transportaba directamente de un extremo a otro del laberinto, a pesar de haber configurado los límites de modo correcto.
    \item Al retroceder su camino, si había varias celdas vecinas ya visitadas que pertenecían al mismo camino, se confundía de camino.
\end{enumerate}

Al revisar el código no parecía haber errores de programación, pero evidentemente los había. De modo que se recurrió a la siguiente estrategia de debugging.

Se creó un método interno llamado \textbf{status} que imprime por consola, en formato de texto, información relevante cada vez que la rata visita una nueva celda. Luego, cada vez que se ejecuta el programa, se desvía el output a un archivo de texto, del siguiente modo:

\begin{lstlisting}[language=Bash]
=> python3 tplaberinto.py > rata_ouptut
\end{lstlisting}

Se procede a observar la rata recorriendo un laberinto en la interfaz gráfica, a la mínima velocidad posible, prestando atención a la zona aproximada donde comete un error. Una vez observado, se cierra el programa, se abre el archivo de texto y se lo recorre hasta encontrar este punto.

Esta simple estrategia sirvió esencialmente para resolver todos los problemas.

El código de \textbf{status} se detalla a continuación:

\begin{lstlisting}[language=Python]
def _status(self):
    va = self._vecinasAccesibles()
    vanv = self._vecinasAccesiblesNoVisitadas()
    vav = self._vecinasAccesiblesVisitadas()
    print('rata: ' + str(self.rata))
    print('accesibles: ' + str(va))
    print('accesibles no visitadas: ' + str(vanv))
    print('accesibles visitadas: ' + str(vav))
    print('camino: ' + str(self.caminoActual))
    print('-'*60)
\end{lstlisting}

A modo de muestra, en un experimento este código produjo el siguiente output:

\begin{lstlisting}[language=Bash]
rata: (0, 0)
accesibles: [(0, 1), (1, 0)]
accesibles no visitadas: [(0, 1), (1, 0)]
accesibles visitadas: []
camino: [(0, 0)]
------------------------------------------------------------
rata: (0, 1)
accesibles: [(0, 0), (0, 2)]
accesibles no visitadas: [(0, 2)]
accesibles visitadas: [(0, 0)]
camino: [(0, 0), (0, 1)]
------------------------------------------------------------
rata: (0, 2)
accesibles: [(0, 1), (0, 3)]
accesibles no visitadas: [(0, 3)]
accesibles visitadas: [(0, 1)]
camino: [(0, 0), (0, 1), (0, 2)]
------------------------------------------------------------
rata: (0, 3)
accesibles: [(0, 2), (0, 4), (1, 3)]
accesibles no visitadas: [(0, 4), (1, 3)]
accesibles visitadas: [(0, 2)]
camino: [(0, 0), (0, 1), (0, 2), (0, 3)]
------------------------------------------------------------
\end{lstlisting}



\newpage
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.8\linewidth}
        \centering
        \includegraphics[width=\linewidth]{conIter.png}
        \caption{Incluyendo algoritmo iterativo}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}[b]{0.8\linewidth}
        \centering
        \includegraphics[width=\linewidth]{sinIter.png}
        \caption{Sin incluir algoritmo iterativo}
    \end{subfigure}
    \par\bigskip
    \caption{Rendimiento de algoritmos del Ejercicio 1}
\end{figure}



\newpage
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=\linewidth]{imgOriginal.jpg}
        \caption{Imagen original}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=\linewidth]{imgFiltros.jpg}
        \caption{Imagen luego de aplicar filtros}
    \end{subfigure}
    \par\bigskip
    \caption{Aplicación de filtros del Ejercicio 2}
\end{figure}



\newpage
\begin{figure}
    \centering
    \begin{subfigure}[b]{\linewidth}
        \centering
        \includegraphics[width=\linewidth]{paral.png}
    \end{subfigure}
    \par\bigskip
    \caption{Rendimiento de los filtros del Ejercicio 2, con distinta cantidad de núcleos}
\end{figure}



\end{document}
